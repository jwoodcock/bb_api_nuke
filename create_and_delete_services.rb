def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  Time.zone = 'London'
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ STARTED +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end


setup
api_login

get "admin/#{child_company_id}/services?per_page=10", @auth_headers
puts "before create: #{json_body[:total_entries]} services present"
puts "creating 5 services"
5.times do
  payload = {name: SecureRandom.hex}
  post "admin/#{child_company_id}/services", payload, @auth_headers
end
sleep(10)
get "admin/#{child_company_id}/services?per_page=10", @auth_headers
puts "after create: #{json_body[:total_entries]} services present"
puts "deleting 10 services"
json_body[:_embedded][:services].each do |service|
  delete "admin/#{child_company_id}/services/#{service[:id]}", {}, @auth_headers
end
get "admin/#{child_company_id}/services?per_page=10", @auth_headers
puts "after delete: #{json_body[:total_entries]} services present"
