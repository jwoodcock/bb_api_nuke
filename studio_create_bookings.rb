require 'selenium/webdriver'
require 'webdrivers'
require 'capybara/dsl'
require 'dotenv'
require 'airborne'
require 'rspec'
require 'faker'
require 'pry'
require 'active_support/all'
require 'logger'
include Capybara::DSL
include Airborne
Dotenv.load

def setup
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver
    .new(
      app,
      browser: :chrome,
      options: Selenium::WebDriver::Chrome::Options.new(
        args: %w[--disable-gpu --no-sandbox headless --disable-popup-blocking --window-size=1920,1080 --enable-features=NetworkService,NetworkServiceInProcess]
      )
    )
  end

  Capybara.default_driver = :chrome
  Capybara.default_max_wait_time = 20

  Capybara.current_driver = :chrome

  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
  print "+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n"

  api_login
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def create_admin
  password = SecureRandom.hex(8)
  admin = {
    name: "#{Faker::Name.first_name.downcase.capitalize} #{Faker::Name.last_name.downcase.capitalize}",
    email: Faker::Internet.safe_email,
    password: password,
    password_confirmation: password,
    phone: Faker::PhoneNumber.phone_number.delete(' x()-'),
    role: 'admin'
  }
  post "admin/#{child_company_id}/administrators", admin, @auth_headers
  admin
end

def create_schedule
  post "admin/#{child_company_id}/schedules", {name: "#{Faker::Name.first_name} #{rand(100..999)} schedule"}, @auth_headers
  json_body[:id]
end

def generic_shift_payload
  start_time = "09:00"
  end_time = "17:00"
  {
    start_date: Date.today.strftime("%Y-%m-%dT#{start_time}:00"),
    start_time: start_time,
    end_time: end_time,
    by_day: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
    end_type: "never",
    repeats: "weekly",
    frequency: "weekly",
    interval: 1
  }
end

def morning_shift_payload
  # start_time = ["07:00", "07:15", "07:30", "07:45", "08:00"].sample
  # end_time = ["11:00", "11:15", "11:30", "11:45", "12:00"].sample
  start_time = "09:00"
  start_time = "17:00"
  {
    start_date: Date.today.strftime("%Y-%m-%dT#{start_time}:00"),
    start_time: start_time,
    end_time: end_time,
    by_day: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
    end_type: "never",
    repeats: "weekly",
    frequency: "weekly",
    interval: 1
  }
end

def afternoon_shift_payload
  start_time = ["12:00", "12:15", "12:30", "12:45", "13:00"].sample
  end_time = ["18:00", "18:15", "18:30", "18:45", "19:00"].sample
  {
    start_date: Date.today.strftime("%Y-%m-%dT#{start_time}:00"),
    start_time: start_time,
    end_time: end_time,
    by_day: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
    end_type: "never",
    repeats: "weekly",
    frequency: "weekly",
    interval: 1
  }
end

def create_shift(schedule_id, shift_payload)
  post "admin/#{child_company_id}/schedules/#{schedule_id}/shift_patterns", shift_payload, @auth_headers
end

def create_staff(number_of_staff)
  number_of_staff.times do
    post "admin/#{child_company_id}/people", {name: "#{Faker::Name.first_name}"}, @auth_headers
    person_id = json_body[:id]
    schedule_id = create_schedule
    put "admin/#{child_company_id}/people/#{person_id}", {schedule_id: schedule_id}, @auth_headers
    if ENV['MULTIPLE_SHIFTS_PER_OBJECT'] == 'true'
      create_shift(schedule_id, morning_shift_payload)
      create_shift(schedule_id, afternoon_shift_payload)
    else
      create_shift(schedule_id, generic_shift_payload)
    end
  end
  get "admin/#{child_company_id}/people", @auth_headers
  $logger.info { "#{json_body[:total_entries]} people created" }
  print "#{json_body[:total_entries]} people created\n"
  json_body[:_embedded][:people].map { |person| person[:name] }
end

def create_resources(number_of_resources)
  number_of_resources.times do
    post "admin/#{child_company_id}/resources", {name: "#{Faker::Name.last_name} #{rand(100..999)}"}, @auth_headers
  end
  get "admin/#{child_company_id}/resources", @auth_headers
  $logger.info { "#{json_body[:total_entries]} resources created" }
  print "#{json_body[:total_entries]} resources created\n"
  json_body[:_embedded][:resources].map { |resource| resource[:name] }
end

def create_services(number_of_services)
  number_of_services.times do
    service_payload = {
      name: "#{Faker::Job.title} #{rand(100..999)}",
      duration: ENV['SERVICE_DURATIONS'].split(',').sample.to_i,
      service_type: "fixed_time"
    }
    post "admin/#{child_company_id}/services", service_payload, @auth_headers
  end
  get "admin/#{child_company_id}/services", @auth_headers
  $logger.info { "#{json_body[:total_entries]} services created" }
  print "#{json_body[:total_entries]} services created\n"
  json_body[:_embedded][:services].map { |service| {name: service[:name], duration: service[:duration]} }
end

def login(admin)
  visit(ENV['STUDIO_URL'])
  find('input#site').set(ENV['ENVIRONMENT'])
  find('input#username').set(admin[:email])
  find('input#password').set(admin[:password])
  click_button('Login')
  return unless page.has_content?(admin[:name])
end

def click_button(button_text)
  find('button', text: button_text).click
end

def new_client
  {
    first_name: Faker::Name.first_name.downcase.capitalize,
    last_name: Faker::Name.last_name.downcase.capitalize,
    email: Faker::Internet.safe_email.split('@').join("#{rand(1000..9999).to_s}@"),
  }
end

def add_client
  client = new_client
  find('input#customer-journey-client-search-bar').set("#{client[:first_name]} #{client[:last_name]}")
  sleep(0.5)
  find('div.typeahead__results-wrapper.ng-scope.typeahead__results-wrapper--button').click
  find('input[aria-describedby="email-description"]').set(client[:email])
  find('input#consent').check unless find('input#consent').checked?
  find('input[value="Save and add"]').click
  $logger.info { "Client added" }
  print "Client added\n"
  client
end

def select_service(service_list)
  return unless page.has_css?('li.services-facet__service')
  service_options = find_all('li.services-facet__service').map { |service_option| service_option.text.split("\n")[0] }
  if service_list.map { |service| service[:name] } != service_options
    $logger.info { "Services displayed do not match service list from API" }
    print "Services displayed do not match service list from API\n"
  end
  service = service_options.sample
  return if !service
  find('li.services-facet__service', text: service, match: :prefer_exact).click
  $logger.info { "Service selected" }
  print "Service selected\n"
  service
end

def select_staff(staff_list)
  return unless page.has_css?('div#select_staff_1')
  find('div#select_staff_1').click
  selected_staff = nil
  within(find('ul.ui-select-choices')) do
    staff_options = find_all('span.ui-select-choices-row-inner').map { |staff_option| staff_option.text.split("\n")[0] }
    if staff_list != staff_options
      $logger.info { "People displayed do not match people list from API" }
      print "People displayed do not match people list from API\n"
    end
    selected_staff = staff_options.sample
    return if !selected_staff
    find('span.ui-select-choices-row-inner', text: selected_staff, match: :prefer_exact).click
  end
  return if !selected_staff
  $logger.info { "Staff selected" }
  print "Staff selected\n"
  selected_staff
end

def select_resource(resource_list)
  return unless page.has_css?('div#select_resource_1')
  find('div#select_resource_1').click
  selected_resource = nil
  within(find('ul.ui-select-choices')) do
    resource_options = find_all('span.ui-select-choices-row-inner').map { |resource_option| resource_option.text }
    if resource_list != resource_options
      $logger.info { "Resources displayed do not match resource list from API" }
      print "Resources displayed do not match resource list from API\n"
    end
    selected_resource = resource_options.sample
    break if !selected_resource
    find('span.ui-select-choices-row-inner', text: selected_resource, match: :prefer_exact).click
  end
  return if !selected_resource
  $logger.info { "Resource selected" }
  print "Resource selected\n"
  selected_resource
end

def select_date
  selected_date = Date.today + rand(ENV['DATE_RANGE'].to_i).days
  currently_displayed_date = Date.parse(find('p[role="heading"]', wait: 1).text)
  if Date.parse(selected_date.strftime('%B %Y')) < currently_displayed_date
    times_moved_back = 0
    until page.has_css?('p[role="heading"]', text: selected_date.strftime('%B %Y'), wait: 2)
      find('button.btn.custom-day-picker__button--month-prev').click
      times_moved_back += 1
      sleep(1)
      find('p[role="heading"]', text: (currently_displayed_date - times_moved_back.months).strftime('%B %Y'))
    end
  end
  if Date.parse(selected_date.strftime('%B %Y')) > currently_displayed_date
    times_moved_forward = 0
    until page.has_css?('p[role="heading"]', text: selected_date.strftime('%B %Y'), wait: 2)
      find('button.btn.custom-day-picker__button--month-next').click
      times_moved_forward += 1
      sleep(1)
      find('p[role="heading"]', text: (currently_displayed_date + times_moved_forward.months).strftime('%B %Y'))
    end
  end
  find("button[aria-label=\"#{selected_date.strftime("%A #{selected_date.strftime('%-d').to_i.ordinalize} %B")}\"]").click
  unless page.has_css?("button[aria-label=\"#{selected_date.strftime("%A #{selected_date.strftime('%-d').to_i.ordinalize} %B")}\"][aria-checked=\"true\"]", wait: 1)
    find("button[aria-label=\"#{selected_date.strftime("%A #{selected_date.strftime('%-d').to_i.ordinalize} %B")}\"]").click
  end
  $logger.info { "Date selected" }
  print "Date selected\n"
  selected_date
end

def select_time
  return unless page.has_css?('button[role="checkbox"]')
  time_button = find_all('button[role="checkbox"]').sample
  time_button.click
  $logger.info { "Time selected" }
  print "Time selected\n"
  time_button.text
end

def record_id
  find('h2.confirmation-modal__status').text.split('Booking ref. ')[1]
end

def check_expected_booking(booking_id, client_hash, service_hash, staff_name, resource_name, date, time)
  get "admin/#{child_company_id}/bookings/#{booking_id}", @auth_headers
  return if response.code != 200
  unless json_body[:client_name] == "#{client_hash[:first_name]} #{client_hash[:last_name]}"
    $logger.info { "client_name is incorrect. Expected: #{client_hash[:first_name]} #{client_hash[:last_name]} Got: #{json_body[:client_name]}" }
    print "client_name is incorrect. Expected: #{client_hash[:first_name]} #{client_hash[:last_name]} Got: #{json_body[:client_name]}\n"
  end
  unless json_body[:client_email] == client_hash[:email]
    $logger.info { "client_email is incorrect. Expected: #{client_hash[:email]} Got: #{json_body[:client_email]}" }
    print "client_email is incorrect. Expected: #{client_hash[:email]} Got: #{json_body[:client_email]}\n"
  end
  unless json_body[:service_name] == service_hash[:name]
    $logger.info { "service_name is incorrect. Expected: #{service_name} Got: #{json_body[:service_name]}" }
    print "service_name is incorrect. Expected: #{service_name} Got: #{json_body[:service_name]}\n"
  end
  unless json_body[:person_name] == staff_name
    $logger.info { "person_name is incorrect. Expected: #{staff_name} Got: #{json_body[:person_name]}" }
    print "person_name is incorrect. Expected: #{staff_name} Got: #{json_body[:person_name]}\n"
  end
  unless json_body[:resource_name] == resource_name
    $logger.info { "resource_name is incorrect. Expected: #{resource_name} Got: #{json_body[:resource_name]}" }
    print "resource_name is incorrect. Expected: #{resource_name} Got: #{json_body[:resource_name]}\n"
  end
  unless Time.parse(json_body[:datetime]) == Time.parse("#{date} #{time}")
    $logger.info { "datetime is incorrect. Expected: #{Time.parse("#{date} #{time}")} Got: #{Time.parse(json_body[:datetime])}" }
    print "datetime is incorrect. Expected: #{Time.parse("#{date} #{time}")} Got: #{Time.parse(json_body[:datetime])}\n"
  end
  unless json_body[:duration] == service_hash[:duration]
    $logger.info { "duration is incorrect. Expected: #{service_hash[:duration]} Got: #{json_body[:duration]}" }
    print "duration is incorrect. Expected: #{service_hash[:duration]} Got: #{json_body[:duration]}\n"
  end
  json_body
end

def close_journey
  if page.has_css?('button.journey__close-journey')
    find('button.journey__close-journey').click
    unless page.has_css?('button', text: 'New Booking')
      click_button('Discard')
    end
  else
    visit(ENV['STUDIO_URL'])
  end
end

setup
staff = create_staff(ENV['STAFF_NUMBER'].to_i)
resources = create_resources(ENV['RESOURCE_NUMBER'].to_i)
services = create_services(ENV['SERVICE_NUMBER'].to_i)
admin = create_admin
login(admin)
bookings = 0
attempts = 0
booking_timings = []
$start = Time.now
$logger<<("+-+-+-+-+-+-+-+-+ SETUP COMPLETE #{$start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
print "+-+-+-+-+-+-+-+-+ SETUP COMPLETE #{$start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n"
until Time.now > ($start + ENV['TEST_DURATION'].to_i.minutes)
  attempts += 1
  start_time = Time.now
  close_journey unless page.has_css?('button', text: 'New Booking')
  $logger.info { "Booking attempt number #{attempts} started at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}" }
  print "Booking attempt number #{attempts} started at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}\n"
  click_button('New Booking')
  client = add_client
  click_button('Continue')
  selected_service = select_service(services)
  if !selected_service
    $logger.info { "No services to select!" }
    print "No services to select!\n"
  end
  break if !selected_service
  click_button('Continue')
  selected_staff = select_staff(staff)
  if !selected_staff
    $logger.info { "No staff to select!" }
    print "No staff to select!\n"
  end
  break if !selected_staff
  selected_resource = select_resource(resources)
  if !selected_resource
    $logger.info { "No resource to select!" }
    print "No resource to select!\n"
  end
  break if !selected_resource
  click_button('Continue')
  date = select_date
  if page.has_css?('div.loader-service-new.loader-service-new--md', wait: 0.5)
    sleep(2)
  end
  time = select_time
  if !time
    $logger.info { "No timeslots available for #{date} with #{selected_staff} and #{selected_resource}" }
    print "No timeslots available for #{date} with #{selected_staff} and #{selected_resource}\n"
  end
  break if !time
  click_button('Continue')
  click_button('Book now')
  booking_id = record_id
  end_time = Time.now
  if !booking_id
    $logger.info { "Booking for #{selected_service}, #{selected_staff} and #{selected_resource} at #{time} on #{date} was not created!" }
    print "Booking for #{selected_service}, #{selected_staff} and #{selected_resource} at #{time} on #{date} was not created!\n"
  end
  break if !booking_id
  click_button('Close')
  $logger.info { "Booking attempt number #{attempts} completed at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}" }
  print "Booking attempt number #{attempts} completed at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}\n"
  selected_service = services.detect { |service| service[:name] == selected_service}
  booking = check_expected_booking(booking_id, client, selected_service, selected_staff, selected_resource, date, time)
  if !booking
    $logger.info { "Booking for #{selected_service}, #{selected_staff} and #{select_resource} at #{time} on #{date} was not breaked by the API!" }
    print "Booking for #{selected_service}, #{selected_staff} and #{select_resource} at #{time} on #{date} was not breaked by the API!\n"
  end
  break if !booking
  bookings += 1
  $logger.info { "Booking created for #{selected_service[:name]} with #{selected_staff} and #{selected_resource} at #{time} on #{date} for a duration of #{selected_service[:duration]}" }
  print "Booking created for #{selected_service[:name]} with #{selected_staff} and #{selected_resource} at #{time} on #{date} for a duration of #{selected_service[:duration]}\n"
  booking_time = end_time - start_time
  booking_timings.append(booking_time)
  $logger.info { "Booking journey took #{booking_time} seconds to complete" }
  print "Booking journey took #{booking_time} seconds to complete\n"
end
$logger.info { "#{bookings} created out of #{attempts} attempts" }
print "#{bookings} created out of #{attempts} attempts\n"
$logger.info { "Quickest booking time = #{booking_timings.sort.first} seconds" }
print "Quickest booking time = #{booking_timings.sort.first} seconds\n"
$logger.info { "Slowest booking time response = #{booking_timings.sort.last} seconds" }
print "Slowest booking time response = #{booking_timings.sort.last} seconds\n"
$logger.info { "Average booking time = #{booking_timings.reduce(:+).to_f / booking_timings.size} seconds" }
print "Average booking time = #{booking_timings.reduce(:+).to_f / booking_timings.size} seconds\n"
$logger<<("+-+-+-+-+-+-+-+-+ TEST ENDED #{Time.now.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
print "+-+-+-+-+-+-+-+-+ TEST ENDED #{Time.now.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n"
Capybara.current_session.driver.quit
