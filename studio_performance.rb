def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

setup
trigger_endpoint = ENV['FUNCTION_TRIGGER_ENDPOINT']
studio_url = ENV['STUDIO_URL']
trigger = "#{trigger_endpoint}?studioUrl=#{studio_url}&envUrl=#{ENV['ENVIRONMENT']}"

api_login

# create admins with group ids
group_ids = []
administrators = []
get "admin/#{child_company_id}/groups?type=people", @auth_headers
json_body[:_embedded][:groups].each do |group|
  password = SecureRandom.hex
  admin_payload = {
    "name": Faker::Name.first_name,
    "email": "#{Faker::Name.first_name}#{rand(1000..10000)}@example.com",
    "password": password,
    "password_confirmation": password,
    "role": 'admin',
    "group_id": group[:id]
  }
  post "admin/#{child_company_id}/administrators", admin_payload, @auth_headers
  administrators.append(admin_payload)
end

100.times do
  action = ['check_today_calendar', 'check_week_calendar'].sample
  admin = administrators.sample
  Airborne.configuration.base_url = trigger_endpoint
  get "&email=#{admin[:email]}&password=#{admin[:password]}&action=#{action}", @auth_headers
  binding.pry
end
