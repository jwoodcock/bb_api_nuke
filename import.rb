def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
end

def company_id
  ENV['COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def import_template
  {
    meta: {
      version: "0.0.1",
      created_at: Time.now.strftime('%Y-%m-%dT%H:%M:%S'),
      feed_name: "Feed"
    },
    settings: {
      actions: [
        "create"
      ]
    },
    data: {
      people: [],
      companies: [],
      users: []
    }
  }
end

def company_import
  {
    object_type: "company",
    status: "live",
    name: Faker::Company.name,
    created_at: Time.now.strftime('%Y-%m-%dT%H:%M:%S'),
    last_modified: Time.now.strftime('%Y-%m-%dT%H:%M:%S'),
    external_id: SecureRandom.hex,
    base_company_ref: ENV['GRANDPARENT_REF'],
    member_company_ref: ENV['GRANDPARENT_REF'],
    is_parent: false, 
    parent_external_id: ENV['PARENT_REF'],
    address: {
      line_1: Faker::Address.street_name,
      locality: Faker::Address.city,
      postal_code: Faker::Address.postcode
    }
  }
end

def person_import(company_ref)
  {
    company_external_ids: [company_ref],
    name: Faker::Name.name,
    schedules: {
      schedules_type: "custom",
      shift_patterns: [
        {
          frequency: "weekly",
          start_date: Time.now.strftime('%Y-%m-%dT00:00:00'),
          start_time: "09:00",
          end_time: "17:00",
          by_day: [
            "monday",
            "tuesday",
            "wednesday",
            "thursday",
            "friday",
            "saturday",
            "sunday"
          ],
          object_type: "shift_pattern",
          company_external_ids: [company_ref]
        },
      object_type: "schedule"
    },
    status: "live",
    external_id: SecureRandom.hex,
    object_type: "person",
    created_at: Time.now.strftime('%Y-%m-%dT%H:%M:%S'),
    last_modified: Time.now.strftime('%Y-%m-%dT%H:%M:%S')
  }
end

setup
importer_payload = import_template
ENV['COMPANY_NUMBER'].to_i.times do
  company = company_import
  importer_payload[:data][:companies].append(company)
  ENV['STAFF_NUMBER'].to_i.times do
    importer_payload[:data][:people].append(person_import(company[:external_id]))
  end
end
api_login
post "admin/#{company_id}/imports", importer_payload, @auth_headers
