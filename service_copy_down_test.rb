def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  Time.zone = 'London'
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ STARTED +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def new_schedule
  {
    rules: {
      '0': '0900-1700',
      '1': '0900-1700',
      '2': '0900-1700',
      '3': '0900-1700',
      '4': '0900-1700',
      '5': '0900-1700',
      '6': '0900-1700'
    }
  }
end

def create_companies
  companies = []
  ENV['COMPANY_NUMBER'].to_i.times do
    post "admin/#{company_id}/company/children", {name: "#{Faker::Company.name} #{rand(1000..9999)}", timezone: 'Europe/London'}, @auth_headers
    companies.append(json_body[:id])
  end
  companies
end

def create_staff(companies)
  staff = []
  companies.each do |company|
    ENV['STAFF_NUMBER'].to_i.times do
      post "admin/#{company}/people", {name: Faker::Name.name, schedule: new_schedule}, @auth_headers
      staff.append(json_body[:id])
    end
  end
  staff
end

def create_resources(companies)
  resources = []
  companies.each do |company|
    ENV['RESOURCE_NUMBER'].to_i.times do
      post "admin/#{company}/resources", {name: Faker::Name.name, schedule: new_schedule}, @auth_headers
      resources.append(json_body[:id])
    end
  end
  resources
end

def create_service
  service_payload = {
    name: "#{Faker::Job.title} #{rand(100..999)}",
    duration: 60,
    service_type: "fixed_time"
  }
  post "admin/#{company_id}/services", service_payload, @auth_headers
  json_body
end

setup
api_login
companies = create_companies
$logger.info { "#{companies.count} companies created" }
staff = create_staff(companies)
$logger.info { "#{staff.count} staff created" }
resources = create_resources(companies)
$logger.info { "#{resources.count} resources created" }
sleep(300)
service = create_service
start_time = Time.now
$logger.info { "Service copy down started at #{start_time.strftime('%Y-%m-%dT%H:%M:%S')}" }
get "admin/#{company_id}/auditlog/history/service/#{service[:id]}", @auth_headers
parent_service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= start_time }
while parent_service_data == []
  sleep(300)
  get "admin/#{company_id}/auditlog/history/service/#{service[:id]}", @auth_headers
  parent_service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= start_time }
end
copy_down_complete_time = Time.zone.parse(parent_service_data[0][:datetime])
$logger.info { "Service copy down completed at #{copy_down_complete_time.strftime('%Y-%m-%dT%H:%M:%S')}" }
child_service_links = parent_service_data[0][:original][:_links][:child_services]
if child_service_links.count == companies.count
  $logger.info { "All #{child_service_links.count} child companies have the service" }
else
  $logger.info { "#{child_service_links.count} out of #{companies.count} child companies have the service" }
end
audit_log_links = []
child_service_links.each do |link|
  child_company_id = link[:href].split('admin/')[1].split('/services')[0]
  child_service_id = link[:href].split('services/')[1]
  audit_log_links.append("admin/#{child_company_id}/auditlog/history/service/#{child_service_id}")
end
get audit_log_links.last, @auth_headers
last_child_service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= copy_down_complete_time }
while last_child_service_data == []
  sleep(300)
  get audit_log_links.last, @auth_headers
  last_child_service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= copy_down_complete_time }
end
who_what_where_update_complete_times = []
audit_log_links.each do |link|
  get link, @auth_headers
  service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= copy_down_complete_time }
  while service_data == []
    sleep(30)
    get link, @auth_headers
    service_data = json_body[:data].select { |data| Time.zone.parse(data[:datetime]) >= copy_down_complete_time }
  end
  who_what_where_update_complete_times.append(Time.zone.parse(service_data[0][:datetime]))
end
final_www_update_time = who_what_where_update_complete_times.sort.last
$logger.info { "Who What Where updates completed at #{final_www_update_time.strftime('%Y-%m-%dT%H:%M:%S')}" }
$logger<<("+-+-+-+-+-+-+-+-+ FINISHED +-+-+-+-+-+-+-+-+\n")
