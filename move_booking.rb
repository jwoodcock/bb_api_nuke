def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def service_list
  get "#{child_company_id}/services", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if !json_body
  json_body
end

def times(service_id, start_date, end_date)
  get "#{child_company_id}/times?start_date=#{start_date}&end_date=#{end_date}&service_id=#{service_id}&person_id=16069", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 200
  json_body
end

def add_item(service_id, datetime)
  post "#{child_company_id}/basket/add_item", {service_id: service_id, datetime: datetime}, public_headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  response
end

def checkout(client, headers)
  post "#{child_company_id}/basket/checkout", {client: client}, headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  json_body
end

def client_details
  {
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.safe_email
  }
end

def move_booking(booking_id, datetime, headers)
  put "admin/#{child_company_id}/bookings/#{booking_id}", {datetime: datetime}, headers
  puts response.code
end

setup
api_login
# service_list_response = service_list
# service = service_list_response[:_embedded][:services].sample[:id]
# start_date = Date.today
# end_date = Date.today + 7
# times_response = times(service, start_date, end_date)
# booking_time = times_response[:times].map { |n| n if n[:available] == true }.compact.sample[:start]
# basket = add_item(service, booking_time)
# basket_headers = public_headers.merge(auth_token: basket.headers[:auth_token])
# booking = checkout(client_details, basket_headers)
# booking_id = booking[:_embedded][:bookings][0][:id]
get "admin/#{child_company_id}/bookings/14669", @auth_headers
booking_id = json_body[:id]
service = json_body[:service_id]
start_date = Date.today
end_date = Date.today + 7
times_response = times(service, start_date, end_date)
times = times_response[:times].map { |n| n if n[:available] == true }.compact
50.times do
  booking_time = times.sample[:start]
  move_booking(booking_id, booking_time, @auth_headers)
end