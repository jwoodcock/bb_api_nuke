require 'airborne'
require 'dotenv'
require 'rspec'
require 'faker'
require 'pry'

Dotenv.load
include Airborne

def setup
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
end

def parent_company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_creds
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def login
  post "login/admin/#{parent_company_id}", login_creds, public_headers

  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def get_groups(company_id)
  get "admin/#{company_id}/groups?type=people", @auth_headers
end

def admin_payload(group_id)
  password = SecureRandom.hex
  {
    name: Faker::Name.first_name,
    email: "#{Faker::Name.first_name}#{rand(1000..9999)}@example.com",
    password: password,
    password_confirmation: password,
    role: 'admin',
    group_id: group_id
  }
end

def callcentre_payload
  password = SecureRandom.hex
  {
    name: Faker::Name.first_name,
    email: "#{Faker::Name.first_name}#{rand(1000..9999)}@example.com",
    password: password,
    password_confirmation: password,
    role: 'callcenter'
  }
end

def create_admin(payload)
  post "admin/#{child_company_id}/administrators", payload, @auth_headers
end

setup
login
# get_groups(child_company_id)
# File.open("admins.txt", "w+") do |file|
#   json_body[:_embedded][:groups].each do |group|
#     payload = admin_payload(group[:id])
#     create_admin(payload)
#     if response.code == 201
#       file.write("#{payload[:email]}, #{payload[:password]}, #{group[:id]}\n")
#     else
#       file.write("#{payload[:email]}, #{payload[:password]}, #{group[:id]} FAILED\n")
#     end
#   end
# end

# File.open("callcentreusers.txt", "w+") do |file|
#   10.times do
#     payload = callcentre_payload
#     create_admin(payload)
#     if response.code == 201
#       file.write("#{payload[:email]}, #{payload[:password]}\n")
#     else
#       file.write("#{payload[:email]}, #{payload[:password]} FAILED\n")
#     end
#   end
# end

def shift_payload
  {
    start_date: "2023-07-15T09:00:00.000Z",
    start_time: "09:00",
    end_time: "17:00",
    repeats: "weekly",
    shift_pattern_condition_id: nil,
    dates: ["2023-07-23"],
    frequency: "sporadic",
    repeat_count: nil
  }
end

get "admin/#{child_company_id}/people?page=1&per_page=10000", @auth_headers
json_body[:_embedded][:people].each do |person|
  post "admin/#{child_company_id}/schedules/#{person[:schedule_id]}/shift_patterns", shift_payload, @auth_headers
  if response.code == 201
    puts 'passed'
  else
    puts 'failed'
  end
end

# get "admin/#{child_company_id}/people?page=1&per_page=10000", @auth_headers
# json_body[:_embedded][:people].each do |person|
#   if person[:service_ids].include?(10662)
#     puts 'yes'
#   else
#     puts 'no'
#     service_ids = person[:service_ids]
#     service_ids.append(10662)
#     put "admin/#{child_company_id}/people/#{person[:id]}", {service_ids: service_ids}, @auth_headers
#     puts 'updated' if response.code == 200 && json_body[:service_ids].include?(10662)
#   end
# end
