def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def new_schedule
  {
    rules: {
      '0': '0900-1700',
      '1': '0900-1700',
      '2': '0900-1700',
      '3': '0900-1700',
      '4': '0900-1700',
      '5': '0900-1700',
      '6': '0900-1700'
    }
  }
end

def create_staff(number_of_staff)
  number_of_staff.times do
    post "admin/#{child_company_id}/people", {name: "#{Faker::Name.first_name} #{rand(100..999)}", schedule: new_schedule}, @auth_headers
  end
  get "admin/#{child_company_id}/people", @auth_headers
  $logger.info { "#{json_body[:total_entries]} people created" }
  json_body[:_embedded][:people].map { |person| person[:name] }
end

def create_resources(number_of_resources)
  number_of_resources.times do
    post "admin/#{child_company_id}/resources", {name: "#{Faker::Name.last_name} #{rand(100..999)}", schedule: new_schedule}, @auth_headers
  end
  get "admin/#{child_company_id}/resources", @auth_headers
  $logger.info { "#{json_body[:total_entries]} resources created" }
  json_body[:_embedded][:resources].map { |resource| resource[:name] }
end

def create_services(number_of_services)
  number_of_services.times do
    service_payload = {
      name: "#{Faker::Job.title} #{rand(100..999)}",
      duration: 60,
      service_type: "fixed_time"
    }
    post "admin/#{child_company_id}/services", service_payload, @auth_headers
  end
  get "admin/#{child_company_id}/services", @auth_headers
  $logger.info { "#{json_body[:total_entries]} services created" }
  json_body[:_embedded][:services].map { |service| {name: service[:name], duration: service[:duration]} }
end

def service_list
  get "#{child_company_id}/services", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if !json_body
  json_body
end

def time_data(service, start_date, end_date, duration)
  get "#{child_company_id}/time_data?date=#{start_date}&end_date=#{end_date}&service_id=#{service[:id]}&duration=#{duration}", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 200
  json_body
end

def add_item(service, datetime, duration)
  post "#{child_company_id}/basket/add_item", {service_id: service[:id], datetime: datetime, duration: duration}, public_headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  response
end

def checkout(client, headers)
  post "#{child_company_id}/basket/checkout", {client: client}, headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  json_body
end

def client_details
  {
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    email: Faker::Internet.safe_email
  }
end

def cancel_booking(id, headers)
  delete "admin/#{child_company_id}/bookings/#{id}", nil, headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 200
end

setup
# api_login
# staff = create_staff(ENV['STAFF_NUMBER'].to_i)
# resources = create_resources(ENV['RESOURCE_NUMBER'].to_i)
# services = create_services(ENV['SERVICE_NUMBER'].to_i)
bookings = 0
attempts = 0
@booking_ids = []
@time_data_timings = []
service_list_response = service_list
service = service_list_response[:_embedded][:services].sample
duration = service[:durations].first
start_date = Date.today
end_date = Date.today + 7
time_data_response = time_data(service, start_date, end_date, duration)
events = time_data_response[:_embedded][:events].select { |event| event[:times].empty? == false }
available_times = []
if events.count > 0
  events.each do |times|
    times[:times].each do |time|
      available_times.append(time[:datetime]) if time[:avail] == 1
    end
  end
end
$start = Time.now
$logger<<("+-+-+-+-+-+-+-+-+ SETUP COMPLETE #{$start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
until Time.now > ($start + ENV['TEST_DURATION'].to_i.minutes)
# until bookings == 10
  attempts += 1
  $logger.info { "Booking started at #{Time.now}" }
  service = service_list_response[:_embedded][:services].sample
  duration = service[:durations].sample
  booking_time = available_times.sample
  basket = add_item(service, booking_time, duration)
  $logger.info { "Add item unsuccessful for service #{service} at #{booking_time}" } if !basket
  if basket
    basket_headers = public_headers.merge(auth_token: basket.headers[:auth_token])
    booking = checkout(client_details, basket_headers)
    if booking
      $logger.info { "Booking complete for service #{service} for #{booking_time} at #{Time.now}"}
      puts booking[:_embedded][:bookings][0][:id]
      bookings += 1
    end
  end
end
$logger.info { "#{bookings} created out of #{attempts} attempts" }
$logger.info { "Quickest time_data response = #{@time_data_timings.sort.first} seconds" }
$logger.info { "Slowest time_data response = #{@time_data_timings.sort.last} seconds" }
$logger.info { "Average time_data response = #{@time_data_timings.reduce(:+).to_f / @time_data_timings.size} seconds" }
$logger<<("+-+-+-+-+-+-+-+-+ TEST ENDED #{Time.now} +-+-+-+-+-+-+-+-+\n")
