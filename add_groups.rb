def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def people_per_group
  ENV['PEOPLE_PER_GROUP'].to_i
end

def specific_child
  return true if ENV['SPECIFIC_CHILD'] == 'true'
  false
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

setup
api_login

child_companies = []
if specific_child
  child_companies = [child_company_id]
else
  get "admin/#{company_id}/company/children", @auth_headers
  child_companies = json_body[:_embedded][:companies].map { |company| company[:id] }
end

number_of_groups = 0
child_companies.each do |company|
  get "admin/#{company}/people?per_page=1", @auth_headers
  number_of_groups = (json_body[:total_entries].to_f / people_per_group).ceil if (json_body[:total_entries].to_f / people_per_group).ceil > number_of_groups
end

groups = []
number_of_groups.times do
  group_payload = {
    name: Faker::Address.city
  }
  post "admin/#{company_id}/groups", group_payload, @auth_headers
  groups.append(json_body[:id])
end

child_companies.each do |company|
  groups.each_with_index do |group, index|
    get "admin/#{company}/people?per_page=#{people_per_group}&page=#{index + 1}", @auth_headers
    if response.code == 200
      if json_body[:_embedded][:people].count > 0
        json_body[:_embedded][:people].each do |person|
          put "admin/#{company}/people/#{person[:id]}", {group_id: group}, @auth_headers
        end
      end
    end
  end
end
