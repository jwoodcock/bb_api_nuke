def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne

  Airborne.configure do |config|
    config.base_url = "https://#{ENV['ENVIRONMENT']}.bookingbug.com/api/#{ENV['API_VERSION']}/"
  end
  $start = Time.now
  $logger = Logger.new("logs/main/#{$start.strftime("%Y-%m-%d %H:%M")}_main.log")
  $error_api = Logger.new("logs/error_api/#{$start.strftime("%Y-%m-%d %H:%M")}_error_api.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$start.strftime("%Y-%m-%d %H:%M")} +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def login_creds
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def login
  post "login/admin/#{company_id}", login_creds, public_headers

  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def queue_payload
  {
    "first_name": Faker::Name.first_name,
    "consent": true,
    "service_id": "48426",
    "locale": "en"
  }
end

def add_to_queue
  post "admin/#{company_id}/queuers?live_status=true", queue_payload, @auth_headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if !json_body
  json_body
end

setup
login
50.times do
  add_to_queue
end
