def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  Time.zone = 'London'
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ STARTED +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end


setup
api_login

get "admin/#{company_id}/services?page=1&per_page=1", @auth_headers
@service = json_body[:_embedded][:services][0]
i = 1
@company_ids = (37166..40421).to_a

payload = {
  companies: []
}
@company_ids.each do |id|
  payload[:companies].append({id: id, enabled: false})
end
patch "admin/#{company_id}/services/#{@service[:id]}/companies", payload, @auth_headers
