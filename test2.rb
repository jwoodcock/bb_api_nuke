options = [
  '{{slot.member.name}}',
  '{{slot.member.email}}',
  '{{slot.person.name}}',
  '{{slot.resource.name}}',
  '{{slot.date_time}}'
]

n = 200000

File.open("text.txt", "w+") do |file|
  n.times do
    file.write("#{options.sample} ")
  end
end
