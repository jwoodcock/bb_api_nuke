require 'rubygems'
require 'selenium-webdriver'
# Input capabilities
capabilities = {
'bstack:options' => {
"osVersion" => "14",
"deviceName" => "iPhone 12",
"realMobile" => "true",
"seleniumVersion" => "4.0.0"
},
}
driver = Selenium::WebDriver.for(:remote,
  :url => "https://jrni1:hpjN9pLjAp32C8gPNt4q@hub-cloud.browserstack.com/wd/hub",
  :desired_capabilities => capabilities)
# Searching for 'BrowserStack' on google.com
driver.navigate.to "http://www.google.com"
element = driver.find_element(:name, "q")
element.send_keys "BrowserStack"
element.submit
wait = Selenium::WebDriver::Wait.new(:timeout => 5) # seconds
begin
  wait.until { !driver.title.match(/BrowserStack/i).nil? }
  driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"passed", "reason": "Yaay! Title matched!"}}')
rescue
  driver.execute_script('browserstack_executor: {"action": "setSessionStatus", "arguments": {"status":"failed", "reason": "Oops! Title did not match"}}')
end
driver.quit