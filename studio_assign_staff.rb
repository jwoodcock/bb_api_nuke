require 'selenium/webdriver'
require 'webdrivers'
require 'capybara/dsl'
require 'dotenv'
require 'airborne'
require 'rspec'
require 'faker'
require 'pry'
require 'active_support/all'
require 'logger'
include Capybara::DSL
include Airborne
Dotenv.load

def setup
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  Capybara.register_driver :chrome do |app|
    Capybara::Selenium::Driver
    .new(
      app,
      browser: :chrome,
      options: Selenium::WebDriver::Chrome::Options.new(
        args: %w[--disable-gpu --no-sandbox --disable-popup-blocking --window-size=1920,1080 headless --enable-features=NetworkService,NetworkServiceInProcess]
      )
    )
  end

  Capybara.default_driver = :chrome
  Capybara.default_max_wait_time = 20

  Capybara.current_driver = :chrome

  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")

  api_login
end

def company_id
  ENV['COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def create_child_companies(number_of_companies)
  number_of_companies.times do
    post "admin/#{company_id}/company/children", {name: "#{Faker::Name.first_name} #{rand(1000..9999)}"}, @auth_headers
    sleep(1)
  end
  get "admin/#{company_id}/company/children?page=1&per_page=1", @auth_headers
  $logger.info { "#{json_body[:total_entries]} companies created" }
  json_body[:total_entries]
end

def new_schedule
  {
    rules: {
      '0': '0900-1700',
      '1': '0900-1700',
      '2': '0900-1700',
      '3': '0900-1700',
      '4': '0900-1700',
      '5': '0900-1700',
      '6': '0900-1700'
    }
  }
end

def create_staff(number_of_staff)
  number_of_staff.times do
    post "admin/#{company_id}/people", {name: "#{Faker::Name.first_name} #{rand(100..999)}", schedule: new_schedule}, @auth_headers
  end
  get "admin/#{company_id}/people?page=1&per_page=25", @auth_headers
  $logger.info { "#{json_body[:total_entries]} people created" }
  json_body[:total_entries]
end

def create_resources(number_of_resources)
  number_of_resources.times do
    post "admin/#{child_company_id}/resources", {name: "#{Faker::Name.last_name} #{rand(100..999)}", schedule: new_schedule}, @auth_headers
  end
  get "admin/#{child_company_id}/resources", @auth_headers
  $logger.info { "#{json_body[:total_entries]} resources created" }
  json_body[:_embedded][:resources].map { |resource| resource[:name] }
end

def create_services(number_of_services)
  number_of_services.times do
    service_payload = {
      name: "#{Faker::Job.title} #{rand(100..999)}",
      duration: 15 * rand(1..4),
      service_type: "fixed_time"
    }
    post "admin/#{company_id}/services", service_payload, @auth_headers
  end
  get "admin/#{company_id}/services?page=1&per_page=25", @auth_headers
  $logger.info { "#{json_body[:total_entries]} services created" }
  json_body[:total_entries]
end

def login(admin)
  visit(ENV['STUDIO_URL'])
  find('input#site').set(ENV['ENVIRONMENT'])
  find('input#username').set(admin[:email])
  find('input#password').set(admin[:password])
  click_button('Login')
  click_button('Administer Parent')
end

def click_button(button_text)
  find('button', text: button_text).click
end

def open_staff_page
  visit("#{ENV['STUDIO_URL']}#/people/all/list")
end

def confirm_staff_page_open(staff)
  sleep(0.5)
  while page.has_css?('svg.loader-service-new__circular', wait: 0.5)
  end
  if staff < 25
    until page.has_css?('button', text: 'Locations', count: staff)
    end
  else
    until page.has_css?('button', text: 'Locations', count: 25)
    end
  end
end

def open_assignment_modal(companies)
  find_all('button', text: 'Locations').sample.click
  page.has_css?('span.treejs-label', count: companies)
end

def assign_companies(companies)
  find_all('span.treejs-label').sample(rand(1..5)).click
end

def close_assignment_modal(staff)
  find('button', text: 'OK').click
  while page.has_css?('svg.loader-service-new__circular', wait: 0.5)
  end
  if staff < 25
    until page.has_css?('button', text: 'Locations', count: staff)
    end
  else
    until page.has_css?('button', text: 'Locations', count: 25)
    end
  end
end

def move_to_next_page
  unless page.has_css?('a[aria-label="Next"][hidden="true"]', wait: 0)
    find('a[aria-label="Next"]').click
  else
    open_staff_page
  end
end

setup
companies = create_child_companies(ENV['COMPANY_NUMBER'].to_i)
staff = create_staff(ENV['STAFF_NUMBER'].to_i)
services = create_services(ENV['SERVICE_NUMBER'].to_i)
sleep(60)
login(login_credentials)
assignments = 0
attempts = 0
@staff_page_opening_timings = []
@staff_assignment_modal_opening_timings = []
@staff_assignment_modal_closing_timings = []
$start = Time.now
$logger<<("+-+-+-+-+-+-+-+-+ SETUP COMPLETE #{$start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
time_before_page_visit = Time.now
open_staff_page
confirm_staff_page_open(staff)
time_after_page_visit = Time.now
@staff_page_opening_timings.append(page_visit_time)
$logger.info { "Staff page took #{page_visit_time} seconds to load" }
until Time.now > ($start + ENV['TEST_DURATION'].to_i.minutes)
  attempts += 1
  open_assignment_modal(companies)
  assignment_modal_open_time = Time.now
  modal_load_time = assignment_modal_open_time - time_after_page_visit
  @staff_assignment_modal_opening_timings.append(modal_load_time)
  $logger.info { "Assignment modal took #{modal_load_time} seconds to load" }
  assign_companies(companies)
  assignment_close_start = Time.now
  close_assignment_modal(staff)
  assignment_close_end = Time.now
  modal_close_time = assignment_close_end - assignment_close_start
  @staff_assignment_modal_closing_timings.append(modal_close_time)
  $logger.info { "Assignment modal took #{modal_close_time} seconds to close and complete assignments" }
  assignments += 1
  $logger.info { "Assignment complete" }
  time_before_page_visit = Time.now
  move_to_next_page
  confirm_staff_page_open(staff)
  time_after_page_visit = Time.now
  @staff_page_opening_timings.append(page_visit_time)
  $logger.info { "New page of staff took #{page_visit_time} seconds to load" }
end
$logger.info { "#{assignments} assignments completed out of #{attempts} attempts" }
$logger.info { "Quickest staff page load time = #{@staff_page_opening_timings.sort.first} seconds" }
$logger.info { "Slowest staff page load time = #{@staff_page_opening_timings.sort.last} seconds" }
$logger.info { "Average staff page load time = #{@staff_page_opening_timings.reduce(:+).to_f / @staff_page_opening_timings.size} seconds" }
$logger.info { "Quickest assignment modal load time = #{@staff_assignment_modal_opening_timings.sort.first} seconds" }
$logger.info { "Slowest assignment modal load time = #{@staff_assignment_modal_opening_timings.sort.last} seconds" }
$logger.info { "Average assignment modal load time = #{@staff_assignment_modal_opening_timings.reduce(:+).to_f / @staff_assignment_modal_opening_timings.size} seconds" }
$logger.info { "Quickest assignment modal close time = #{@staff_assignment_modal_closing_timings.sort.first} seconds" }
$logger.info { "Slowest assignment modal close time = #{@staff_assignment_modal_closing_timings.sort.last} seconds" }
$logger.info { "Average assignment modal close time = #{@staff_assignment_modal_closing_timings.reduce(:+).to_f / @staff_assignment_modal_closing_timings.size} seconds" }
$logger<<("+-+-+-+-+-+-+-+-+ TEST ENDED #{Time.now.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
