def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ STARTED +-+-+-+-+-+-+-+-+\n")
end

def company_id
  ENV['COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

setup
api_login
get "admin/#{company_id}/people", @auth_headers
person_name = json_body[:_embedded][:people].last[:name]
$logger.info { "person name #{person_name}" }
companies = (37082..37581).to_a
companies.each do |company|
  get "admin/#{company}/people", @auth_headers
  person_id = nil
  if response.code == 200 && json_body[:total_entries] > 0
    person = json_body[:_embedded][:people].select { |person| person[:name] == person_name }
    if person == nil
      $logger.info { "people copy down job to company #{company} has not completed" }
    else
      $logger.info { "person was copied to company #{company} #{person.count} times" }
    end
  else
    $logger.info { "person is not present on company #{company}" }
  end
end
$logger<<("+-+-+-+-+-+-+-+-+ FINISHED +-+-+-+-+-+-+-+-+\n")
