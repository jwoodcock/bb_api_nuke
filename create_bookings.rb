require 'dotenv'
require 'airborne'
require 'rspec'
require 'faker'
require 'pry'
require 'active_support/all'
require 'logger'
include Airborne
Dotenv.load

def setup
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end

  api_login
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def create_schedule
  post "admin/#{child_company_id}/schedules", {name: "#{Faker::Name.first_name} #{rand(100..999)} schedule"}, @auth_headers
  json_body[:id]
end

def generic_shift_payload
  start_time = "08:00"
  end_time = "18:00"
  {
    start_date: Date.today.strftime("%Y-%m-%dT#{start_time}:00"),
    start_time: start_time,
    end_time: end_time,
    by_day: ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
    end_type: "never",
    repeats: "weekly",
    frequency: "weekly",
    interval: 1
  }
end

def create_shift(schedule_id, shift_payload)
  post "admin/#{child_company_id}/schedules/#{schedule_id}/shift_patterns", shift_payload, @auth_headers
end

def create_staff(number_of_staff)
  number_of_staff.times do
    post "admin/#{child_company_id}/people", {name: "#{Faker::Name.first_name}"}, @auth_headers
    person_id = json_body[:id]
    schedule_id = create_schedule
    put "admin/#{child_company_id}/people/#{person_id}", {schedule_id: schedule_id}, @auth_headers
    create_shift(schedule_id, generic_shift_payload)
  end
end

def create_resources(number_of_resources)
  number_of_resources.times do
    post "admin/#{child_company_id}/resources", {name: "#{Faker::Name.first_name}"}, @auth_headers
    resource_id = json_body[:id]
    schedule_id = create_schedule
    put "admin/#{child_company_id}/resources/#{resource_id}", {schedule_id: schedule_id}, @auth_headers
    create_shift(schedule_id, generic_shift_payload)
  end
end

def new_client
  {
    first_name: Faker::Name.first_name.downcase.capitalize,
    last_name: Faker::Name.last_name.downcase.capitalize,
    email: Faker::Internet.email.split('@').join("#{rand(1000..9999).to_s}@"),
  }
end

def add_item(service_id, datetime)
  post "#{child_company_id}/basket/add_item", {service_id: service_id, datetime: datetime}, public_headers
  return if response.code != 201
  response
end

def checkout(client, headers)
  post "#{child_company_id}/basket/checkout", {client: client}, headers
  return if response.code != 201
  json_body
end

setup
# staff = create_staff(10)
# create_resources(5)

get "admin/#{child_company_id}/services", @auth_headers
service_ids = json_body[:_embedded][:services].map {|s| s[:id] }

first_date = Date.parse('1 January 2024')
last_date = Date.parse('1 February 2024')

booking_date = first_date.dup
datetimes = []
while booking_date < last_date
  [
    booking_date.strftime('%Y-%m-%dT08:00:00'),
    booking_date.strftime('%Y-%m-%dT09:00:00'),
    booking_date.strftime('%Y-%m-%dT10:00:00'),
    booking_date.strftime('%Y-%m-%dT11:00:00'),
    booking_date.strftime('%Y-%m-%dT12:00:00'),
    booking_date.strftime('%Y-%m-%dT13:00:00'),
    booking_date.strftime('%Y-%m-%dT14:00:00'),
    booking_date.strftime('%Y-%m-%dT15:00:00'),
    booking_date.strftime('%Y-%m-%dT16:00:00'),
    booking_date.strftime('%Y-%m-%dT17:00:00')
  ].each do |time|
    datetimes.append(time)
  end
  booking_date += 1.days
end

datetimes.each do |time|
  basket = add_item(service_ids[1], time)
  if basket
    basket_headers = public_headers.merge(auth_token: basket.headers[:auth_token])
    booking = checkout(new_client, basket_headers)
    if booking == nil
      puts "unable to book at #{time}"
    end
  else
    puts "unable to book at #{time}"
  end
end

puts 'Staff'
get "admin/#{child_company_id}/people", @auth_headers
booking_count = json_body[:_embedded][:people].map do |person|
  get "admin/#{child_company_id}/bookings?start_date=2023-12-01&person_id=#{person[:id]}&include_cancelled=false", @auth_headers
  {person: person[:name], bookings: json_body[:total_entries]}
end
total_bookings = booking_count.sum {|b| b[:bookings] }
booking_count.each do |b|
  percentage = b[:bookings].to_f / total_bookings * 100
  puts "#{b[:person]} booking count: #{b[:bookings]}, percentage: #{percentage}%"
end

puts ''
puts "Resources"
get "admin/#{child_company_id}/resources", @auth_headers
booking_count = json_body[:_embedded][:resources].map do |resource|
  get "admin/#{child_company_id}/bookings?start_date=2023-12-01&resource_id=#{resource[:id]}&include_cancelled=false", @auth_headers
  {resource: resource[:name], bookings: json_body[:total_entries]}
end
total_bookings = booking_count.sum {|b| b[:bookings] }
booking_count.each do |b|
  percentage = b[:bookings].to_f / total_bookings * 100
  puts "#{b[:resource]} booking count: #{b[:bookings]}, percentage: #{percentage}%"
end
