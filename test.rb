def setup
  require 'airborne'
  require 'dotenv'
  require 'rspec'
  require 'faker'
  require 'pry'
  require 'active_support/all'
  require 'logger'

  Dotenv.load
  include Airborne
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
  $logger = Logger.new("logs/main/#{$setup_start.strftime("%Y-%m-%d_%H:%M")}_main.log")
  $logger<<("+-+-+-+-+-+-+-+-+ NEW TEST STARTED #{$setup_start.strftime("%Y-%m-%d %H:%M:%S")} +-+-+-+-+-+-+-+-+\n")
end

def parent_company_id
  37067
end

def child_company_ids
  [37074]
  #, 37072, 37073, 37074, 37075, 37080, 37081]
end

def login_credentials
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def api_login
  post "login/admin/#{parent_company_id}", login_credentials, public_headers
  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def service_list(company_id)
  get "#{company_id}/services", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if !json_body
  json_body
end

def time_data(company_id, service_id, start_date, end_date)
  get "#{company_id}/time_data?date=#{start_date}&end_date=#{end_date}&service_id=#{service_id}&person_id=15466", public_headers
  if response.code != 200
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 200
  json_body
end

def add_item(company_id, service_id, datetime)
  post "#{company_id}/basket/add_item", {service_id: service_id, datetime: datetime, person_id: 15466}, public_headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  response
end

def checkout(company_id, client, headers)
  post "#{company_id}/basket/checkout", {member_id: client}, headers
  if response.code != 201
    $logger.info ("#{__method__} Failed") { "{response_code: #{response.code}, response_body: #{response.body}}" }
  else
    $logger.info ("#{__method__} Successful")
  end
  return if response.code != 201
  response
end

def client_id
  123
end

setup
api_login
child_company_ids.each do |id|
  service_list_response = service_list(id)
  service = service_list_response[:_embedded][:services].sample
  i = 0
  available_times = []
  until available_times.count > 0 || i > ENV['DATE_RANGE'].to_i
    start_date = Date.today + i
    end_date = Date.today + 7 + i
    time_data_response = time_data(id, service[:id], start_date, end_date)
    events = time_data_response[:_embedded][:events].select { |event| event[:times].empty? == false }
    events.each do |times|
      times[:times].each do |time|
        available_times.append(time[:datetime]) if time[:avail] == 1
      end
    end
    if available_times == []
      i += 7
    end
  end
  booking_time = available_times.sample
  basket = add_item(id, service[:id], booking_time)
  basket_headers = public_headers.merge(auth_token: basket.headers[:auth_token])
  booking = checkout(id, client_id, basket_headers)
end
