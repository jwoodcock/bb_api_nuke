# Stability tests and JRNI API scripts

The following ENV variables are needed:

- ENVIRONMENT - JRNI backend URL to run the script against
- STUDIO_URL - JRNI studio URL to run the script against
- APP_KEY - API app key
- APP_ID - API app id
- EMAIL - Email to login to the company on JRNI
- PASSWORD - Password to login to the company on JRNI
- API_VERSION - Version of the API to run the scripts against
- TEST_DURATION - Duration in minutes for how long the stability test will run for
- COMPANY_ID - Company id for API authentication - must be the company the email and password are associated with
- CHILD_COMPANY_ID - Child company id for the tests to run against
- DATE_RANGE - Number of days from today the bookings will be created between
- STAFF_NUMBER - Number of staff members created for the test
- RESOURCE_NUMBER - Number of resources created for the test
- SERVICE_NUMBER - Number of services created for the test

Run ```ruby <file_name>``` in the terminal.
