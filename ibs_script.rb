require 'dotenv'
require 'airborne'
require 'rspec'
require 'faker'
require 'pry'
require 'active_support/all'
require 'logger'
include Airborne
Dotenv.load

def setup
  $setup_start = Time.now
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
end

def company_id
  ENV['COMPANY_ID']
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def new_client
  {
    first_name: Faker::Name.first_name.downcase.capitalize,
    last_name: "IbsTest",
    email: "IbsTest#{rand(1000..9999)}@example.com"
  }
end

def add_item(service_id, datetime, staff_id)
  post "#{company_id}/basket/add_item", {service_id: service_id, datetime: datetime, person_id: staff_id}, public_headers
  return if response.code != 201
  response
end

def checkout(client, headers)
  post "#{company_id}/basket/checkout", {client: client}, headers
  return if response.code != 201
  json_body
end

def get_times(service_id, staff_id, start_date, end_date, headers)
  get "#{company_id}/times?start_date=#{start_date}&end_date=#{end_date}&service_id=#{service_id}&person_id=#{staff_id}&only_available=true", public_headers
  json_body
end

setup
staff = [15375, 15376, 15548, 15549, 15550, 15551, 15552, 15553]
staff.each do |id|
  response = get_times(48440, id, "2023-12-14", "2023-12-14", public_headers)
  response[:times].each do |time|
    basket = add_item(48440, time[:start], id)
    if basket
      basket_headers = public_headers.merge(auth_token: basket.headers[:auth_token])
      booking = checkout(new_client, basket_headers)
    end
  end
end
