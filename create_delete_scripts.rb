require 'airborne'
require 'dotenv'
require 'rspec'
require 'faker'
require 'pry'

Dotenv.load
include Airborne

def setup
  Airborne.configure do |config|
    config.base_url = "#{ENV['ENVIRONMENT']}/api/#{ENV['API_VERSION']}/"
  end
end

def company_id
  ENV['COMPANY_ID']
end

def child_company_id
  ENV['CHILD_COMPANY_ID']
end

def login_creds
  {
    "email": ENV['EMAIL'],
    "password": ENV['PASSWORD']
  }
end

def public_headers
  {
    'app-key': ENV['APP_KEY'],
    'app-id': ENV['APP_ID']
  }
end

def login
  post "login/admin/#{company_id}", login_creds, public_headers

  @auth_headers = public_headers
  @auth_headers["auth-token"] = json_body[:auth_token]
end

def delete_all_groups
  get "admin/#{company_id}/groups", @auth_headers
  json_body[:_embedded][:groups].each do |group|
    delete "admin/#{company_id}/groups/#{group[:id]}", nil, @auth_headers
  end
end

def delete_all_bookings
  get "admin/#{child_company_id}/bookings?per_page=50", @auth_headers
  while json_body[:_embedded][:bookings].count > 0
    json_body[:_embedded][:bookings].each do |n|
      delete "admin/#{child_company_id}/bookings/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{child_company_id}/bookings?per_page=50", @auth_headers
  end
end

def delete_all_resources
  get "admin/#{child_company_id}/resources?per_page=100", @auth_headers

  while json_body[:_embedded][:resources].count > 0
    json_body[:_embedded][:resources].each do |n|
      delete "admin/#{child_company_id}/resources/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{child_company_id}/resources?per_page=100", @auth_headers
  end
end

def delete_resources(num)
  get "admin/#{child_company_id}/resources?per_page=#{num}", @auth_headers

  json_body[:_embedded][:resources].each do |n|
    delete "admin/#{child_company_id}/resources/#{n[:id]}", nil, @auth_headers
  end
end

def delete_staff(num)
  get "admin/#{child_company_id}/people?per_page=#{num}", @auth_headers

  json_body[:_embedded][:people].each do |n|
    delete "admin/#{child_company_id}/people/#{n[:id]}", nil, @auth_headers
  end
end

def delete_all_staff
  get "admin/#{child_company_id}/people?per_page=100", @auth_headers

  while json_body[:_embedded][:people].count > 0
    json_body[:_embedded][:people].each do |n|
      delete "admin/#{child_company_id}/people/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{child_company_id}/people?per_page=100", @auth_headers
  end
end

def delete_all_customers
  get "admin/#{child_company_id}/client?per_page=100", @auth_headers

  while json_body[:_embedded][:clients].count > 0
    json_body[:_embedded][:clients].each do |n|
      delete "admin/#{child_company_id}/client/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{child_company_id}/client?per_page=100", @auth_headers
  end
end

def delete_all_services
  get "admin/#{company_id}/services?per_page=100", @auth_headers
  until response.to_s == '{"error":"Not Found"}' || json_body[:total_entries] == 0
    json_body[:_embedded][:services].each do |n|
      delete "admin/#{company_id}/services/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{company_id}/services?per_page=100", @auth_headers
  end
end

def delete_all_event_groups
  get "admin/#{child_company_id}/event_groups?per_page=100", @auth_headers

  while json_body[:_embedded][:event_groups].count > 0
    json_body[:_embedded][:event_groups].each do |n|
      delete "admin/#{child_company_id}/event_groups/#{n[:id]}", nil, @auth_headers
    end
    get "admin/#{child_company_id}/event_groups?per_page=100", @auth_headers
  end
end

def create_staff(n)
  n.times do
    payload = {
      name: Faker::Name.first_name,
      email: Faker::Internet.safe_email,
      reference: SecureRandom.hex,
      disabled: [true, false].sample
    }
    post "admin/#{child_company_id}/people", payload, @auth_headers
  end
end

def create_services(n)
  i = 1
  n.times do
    post "admin/#{company_id}/services", {name: "Service #{i} #{Faker::Lorem.paragraphs(number: 3).join}", description: Faker::Lorem.paragraphs(number: 10).join, service_type: 'fixed_time', duration: [60, 30, 15].sample, method_of_appointment: ['In-person', 'Video', 'Phone'].sample}, @auth_headers
    i+=1
  end
end

def update_staff
  get "admin/#{child_company_id}/people", @auth_headers

  json_body[:_embedded][:people].each do |n|
    put "admin/#{child_company_id}/people/#{n[:id]}", {schedule: new_schedule}, @auth_headers
  end
end

def update_resources
  get "admin/#{child_company_id}/resources", @auth_headers

  json_body[:_embedded][:resources].each do |n|
    put "admin/#{child_company_id}/resources/#{n[:id]}", {schedule: new_schedule}, @auth_headers
  end
end

def default_schedule
  {
    rules: {
      '0': '0000-2359',
      '1': '0000-2359',
      '2': '0000-2359',
      '3': '0000-2359',
      '4': '0000-2359',
      '5': '0000-2359',
      '6': '0000-2359'
    }
  }
end

def new_schedule
  {
    rules: {
      '0': '0900-1700',
      '1': '0900-1700',
      '2': '0900-1700',
      '3': '0900-1700',
      '4': '0900-1700',
      '5': '0900-1700',
      '6': '0900-1700'
    }
  }
end

def create_resources(n, company)
  n.times do
    post "admin/#{company}/resources", {name: Faker::Name.first_name, schedule: new_schedule}, @auth_headers
  end
end

def delete_object_mappings
  get "admin/#{child_company_id}/integrations", @auth_headers
  integration_id = json_body[:_embedded][:implementations][0][:id]
  get "admin/#{child_company_id}/integrations/#{integration_id}/object_mappings", @auth_headers
  i = json_body[:_embedded][:object_mappings].count
  while i > 0
    json_body[:_embedded][:object_mappings].each do |n|
      delete "admin/#{child_company_id}/integrations/#{integration_id}/object_mappings/#{n[:id]}", nil, @auth_headers
    end
    i -= 1
  end
end

def resource_import_payload
  {
    meta: {
      version: "0.0.1",
      created_at: "2019-04-11T12:49:28+0100",
      feed_name: "Et maxime molestiae voluptates."
    },
    settings: {
      callback_url: "http://jakubowskifarrell.org/amber_reilly",
      type: "create/update"
    },
    data: {
      people: [],
      companies: [{
        object_type: "company",
        last_modified: "2019-02-01T10:30:10",
        status: "live",
        name: "B&Q Leatherhead",
        resources: [{
          object_type: "resource",
          status: "live",
          name: "Imported resource",
          external_id: "kjwbrkjb3wk49834y98gfwisufb98"
        }],
        external_id: "LEA156"
      }],
      users: []
    }
  }
end

def import_resource
  post "admin/#{child_company_id}/imports", resource_import_payload, @auth_headers
end

def delete_all_companies
  get "#{@api_version}/admin/#{company_id}/company", @auth_headers
  if json_body[:companies]
    json_body[:companies].each do |n|
      delete "#{@api_version}/admin/#{n[:id]}/company", nil, @auth_headers
    end
  end
end

def delete_all_queuers
  get "#{@api_version}/admin/#{child_company_id}/live_status", @auth_headers
  if json_body[:status][:queuers]
    json_body[:status][:queuers].each do |queuer|
      delete "#{@api_version}/admin/#{child_company_id}/queuers/#{queuer[:id]}", nil, @auth_headers
    end
  end
end

def delete_all_slots
  get "admin/#{child_company_id}/slots?start_date=#{Date.today - 2}&end_date=#{Date.today + 365}", @auth_headers
  json_body[:_embedded][:slots].each do |n|
    delete "admin/#{child_company_id}/slots/#{n[:id]}", nil, @auth_headers
  end
end

def find_person(name)
  get "admin/#{company_id}/people?per_page=1&page=1", @auth_headers
  pages = (json_body[:total_entries].to_f / 50).ceil
  i = 1
  people = []
  while i <= pages
    get "admin/#{company_id}/people?per_page=#{50}&page=#{i}", @auth_headers
    json_body[:_embedded][:people].each { |person| people.append(person) if person[:name] == name }
    i += 1
  end
end

def get_staff
  get "admin/#{child_company_id}/people?per_page=1&page=1", @auth_headers
  pages = (json_body[:total_entries].to_f / 25).ceil
  i = 1
  people = []
  while i <= pages
    get "admin/#{child_company_id}/people?per_page=25&page=#{i}", @auth_headers
    json_body[:_embedded][:people].each { |person| people.append(person[:id])}
    i += 1
  end
  people
end

def delete_staff_schedules(ids)
  ids.each do |id|
    get "admin/#{child_company_id}/people/#{id}/schedule", @auth_headers
    if json_body[:id]
      get "admin/#{child_company_id}/schedules/#{json_body[:id]}", @auth_headers
      response = json_body
      if response[:id]
        delete "admin/#{child_company_id}/schedules/#{response[:id]}", nil, @auth_headers
        delete "admin/37124/schedules/#{response[:id]}", nil, @auth_headers
        delete "admin/37120/schedules/#{response[:id]}", nil, @auth_headers
      end
      if response[:_embedded]
        response[:_embedded][:schedules].each do |schedule|
          delete "admin/#{child_company_id}/schedules/#{schedule[:id]}", nil, @auth_headers
          delete "admin/37124/schedules/#{schedule[:id]}", nil, @auth_headers
          delete "admin/37120/schedules/#{schedule[:id]}", nil, @auth_headers
        end
      end
    end
  end
end

def add_schedules(ids)
  ids.each do |id|
    get "admin/#{child_company_id}/people/#{id}/schedule", @auth_headers
    if response.code == 404
      post "admin/#{child_company_id}/schedules", {name: SecureRandom.hex}, @auth_headers
      schedule_id = json_body[:id]
      put "admin/#{child_company_id}/people/#{id}", {schedule_id: schedule_id}, @auth_headers
      payload = {
        start_date: Date.today.strftime('%Y-%m-%dT00:00:00.000Z'),
        start_time: ["08:00", "08:30", "09:00", "09:30", "10:00"].sample,
        end_time: ["16:00", "16:30", "17:00", "17:30", "18:00"].sample,
        by_day: ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"],
        end_type: "never",
        repeats: "weekly",
        frequency: "weekly",
        interval: 1
      }
      post "admin/#{child_company_id}/schedules/#{schedule_id}/shift_patterns", payload, @auth_headers
    end
  end
end

def add_external_blocks(ids, date)
  ids.each do |id|
    day_block = false
    # [0, 0, 0, 1].sample.times do
    #   hours = ["08", "09", "10", "11", "12", "13", "14", "15", "16", "17"]
    #   minutes = ["00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"]
    #   start_time = "#{date}T#{hours.sample}:#{minutes.sample}:00.690Z"
    #   body = {
    #     end_time: (Time.parse(start_time) + [90000, 129600].sample).strftime("%Y-%m-%dT%H:%M:00.000Z"),
    #     start_time: start_time,
    #     updated_at: "#{Time.now.strftime('%Y-%m-%dT%H:%M:%S')}.690Z",
    #     person_id: id
    #   }
    #   put "admin/#{child_company_id}/external_blocks/integration/#{SecureRandom.hex}", body, @auth_headers
    #   day_block = true
    #end
    unless day_block
      rand(1..3).times do
        hours = ["08", "09", "10", "11", "12", "13", "14", "15", "16"]
        minutes = ["00", "05", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55"]
        start_time = "#{date}T#{hours.sample}:#{minutes.sample}:00.690Z"
        body = {
          end_time: (Time.parse(start_time) + [900, 1800, 3600, 7200].sample).strftime("%Y-%m-%dT%H:%M:00.000Z"),
          start_time: start_time,
          updated_at: "#{Time.now.strftime('%Y-%m-%dT%H:%M:%S')}.690Z",
          person_id: id
        }
        put "admin/#{child_company_id}/external_blocks/integration/#{SecureRandom.hex}", body, @auth_headers
      end
    end
  end
end

def delete_bookings_on_date(date)
  get "admin/#{child_company_id}/bookings?start_date=#{date}&end_date=#{date}&per_page=1", @auth_headers
  pages = (json_body[:total_entries].to_f / 100).ceil
  i = 1
  while i <= pages
    get "admin/#{child_company_id}/bookings?start_date=#{date}&end_date=#{date}&per_page=100&page=1", @auth_headers
    json_body[:_embedded][:bookings].each do |booking|
      delete "admin/#{child_company_id}/bookings/#{booking[:id]}", nil, @auth_headers
    end
    i += 1
  end
end

def delete_slots_on_date(date)
  get "admin/#{child_company_id}/slots?start_date=#{date}&end_date=#{date}&per_page=1", @auth_headers
  pages = (json_body[:total_entries].to_f / 100).ceil
  i = 1
  while i <= pages
    get "admin/#{child_company_id}/slots?start_date=#{date}&end_date=#{date}&per_page=100&page=1", @auth_headers
    json_body[:_embedded][:slots].sample(20) do |slot|
      delete "admin/#{child_company_id}/slots/#{slot[:id]}", nil, @auth_headers
    end
    i += 1
  end
end

def return_companies_without_lat_long
  i = 1
  c = 0
  companies = []
  while c <= 4550
    get "company/37000/search?page=#{i}&per_page=50", public_headers
    json_body[:_embedded][:companies].each do |company|
      if company[:address]
        if company[:address][:lat] == nil || company[:address][:long] == nil
          companies.append(company)
        end
      end
    end
    puts i
    puts c
    i += 1
    c += 50
  end
  companies
end

def search_for_client(email)
  query = {
    query: {
      multi_match: {
        query: email,
        fields: [
          "id",
          "name^2",
          "email",
          "mobile",
          "reference"
        ],
        type: "phrase_prefix",
        lenient: true
      }
    }
  }
  post "admin/search/client/?size=10", query, @auth_headers
  return false if json_body[:hits][:total] == 0
  return true if json_body[:hits][:total] > 0
end

def create_events
  payload = {
    name: SecureRandom.hex,
    spaces: rand(10..30),
    event_group_id: 48742,
    events: [{
      duration: 30,
      datetime: "2022-11-#{rand(12..19)}T#{["08:00", "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00"].sample}:00"
    }],
    person_id: 15624,
    extra: {
      mode: "Video"
    }
  }
  post "admin/#{child_company_id}/event_chains", payload, @auth_headers
end

setup
login
# 100.times { create_events }
# delete_all_groups
# create_services(100)
delete_all_bookings
# delete_all_customers
# delete_all_staff
# delete_all_resources
#delete_all_services
# delete_all_companies
# delete_all_queuers
# delete_all_slots
# create_staff(1000)
# delete_staff(3)
# delete_resources(2)
# delete_staff_schedules(staff_ids)
# add_schedules(staff_ids)
# set_fixed_start_times(true)
# delete_bookings_on_date("2023-11-06")
# delete_bookings_on_date("2023-11-07")
# delete_bookings_on_date("2023-11-08")
# delete_bookings_on_date("2023-11-09")
# delete_bookings_on_date("2023-11-10")
# delete_bookings_on_date("2023-11-11")
# delete_bookings_on_date("2023-11-12")
# delete_slots_on_date("2023-07-21")
# delete_slots_on_date("2023-07-22")
# delete_slots_on_date("2023-07-23")
# delete_slots_on_date("2023-07-24")
# delete_slots_on_date("2023-07-25")
# delete_slots_on_date("2023-07-26")
# delete_slots_on_date("2023-07-27")
# delete_slots_on_date("2023-07-28")
# delete_slots_on_date("2023-07-29")
# staff_ids = get_staff
# add_external_blocks(staff_ids, "2022-12-01")
# add_external_blocks(staff_ids, "2022-10-09")
# add_external_blocks(staff_ids, "2022-10-10")
# add_external_blocks(staff_ids, "2022-10-11")
# add_external_blocks(staff_ids, "2022-10-12")
# add_external_blocks(staff_ids, "2022-10-13")
# add_external_blocks(staff_ids, "2022-10-14")

# missing_clients = []
# File.open("returned_clients.txt", "w+") do |file|
#   File.foreach("clients.txt") do |line|
#     email = line.split('"email": "')[1].split('"}}')[0]
#     if search_for_client(email)
#       file.write("#{email}\n")
#     else
#       missing_clients.append(email)
#     end
#   end
# end
# File.open("missing_clients.txt", "w+") do |file|
#   missing_clients.each do |client|
#     file.write("#{client}\n")
#   end
# end

# @auth_headers = {
#   "Accept-Language": "en",
#   "App-Id": "f6b16c23",
#   "App-Key": "f0bc4f65f4fbfe7b4b3b7264b655f5eb",
#   "Auth-Token": "3dsTkRlip_aYyXwEkMpdtA",
#   "Cache-Control": "no-cache",
#   "Connection": "keep-alive",
#   "Cookie": "bbsidUK=d2b5b85c63880ee31822849af4dfe11f; bbsidUS=515d330e13aa74459ac1b28cee6c5826; studio_hosts=%7B%22https%3A%2F%2Ftetris.bookingbug.com%22%3A%7B%22name%22%3A%22tetris%22%2C%22accessed%22%3A%222022-10-21T14%3A55%3A23%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%2C%22https%3A%2F%2Fsf-dev2.bookingbug.com%22%3A%7B%22name%22%3A%22sf-dev2%22%2C%22accessed%22%3A%222022-10-19T15%3A37%3A11%2B00%3A00%22%2C%22environment%22%3A%22staging%22%7D%2C%22https%3A%2F%2Fmorganstanley.bookingbug.com%22%3A%7B%22name%22%3A%22morganstanley%22%2C%22accessed%22%3A%222022-10-28T10%3A16%3A48%2B00%3A00%22%2C%22environment%22%3A%22production%22%7D%2C%22https%3A%2F%2Flts_qa.bookingbug.com%22%3A%7B%22name%22%3A%22lts_qa%22%2C%22accessed%22%3A%222022-10-27T10%3A08%3A00%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%2C%22https%3A%2F%2Ftotalwine.bookingbug.com%22%3A%7B%22name%22%3A%22totalwine%22%2C%22accessed%22%3A%222022-10-21T11%3A37%3A15%2B00%3A00%22%2C%22environment%22%3A%22production%22%7D%2C%22https%3A%2F%2Fedge-frontendqa.bookingbug.com%22%3A%7B%22name%22%3A%22edge-frontendqa%22%2C%22accessed%22%3A%222022-10-27T11%3A59%3A32%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%2C%22https%3A%2F%2Fzelda.bookingbug.com%22%3A%7B%22name%22%3A%22zelda%22%2C%22accessed%22%3A%222022-10-27T11%3A11%3A38%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%2C%22https%3A%2F%2Fedge-template.bookingbug.com%22%3A%7B%22name%22%3A%22edge-template%22%2C%22accessed%22%3A%222022-10-31T09%3A46%3A57%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%2C%22https%3A%2F%2Fedge-qa.bookingbug.com%22%3A%7B%22name%22%3A%22edge-qa%22%2C%22accessed%22%3A%222022-10-27T13%3A04%3A55%2B00%3A00%22%2C%22environment%22%3A%22development%22%7D%7D; Auth-Token=3dsTkRlip_aYyXwEkMpdtA; bbsidmtau=2bc1753dedf0d1c47de561f242676f0b",
#   "Origin": "https://studio.bookingbug.com",
#   "Pragma": "no-cache",
#   "Referer": "https://studio.bookingbug.com/"
#   }
# File.open("booking_times.txt", "w+") do |file|
#   File.foreach("booking_ids.txt") do |line|
#     get "admin/#{company_id}/bookings/#{line.split("\n")[0]}", @auth_headers
#     file.write("#{line.split("\n")[0]} - #{json_body[:updated_at]}\n")
#   end
# end

puts 'Environment cleared!'
